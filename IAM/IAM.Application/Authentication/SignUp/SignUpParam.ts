export interface SignUpParam {
  FirstName: string;
  LastName: string;
  Phone: string;
  Gender: string;
  Picture: string;
}
