export interface VerifyCodeParam {
  Phone: string;
  Code: number;
}
