export interface SignUpRequest {
  firstName: string;
  lastName: string;
  phone: string;
  gender: string;
  picture: string;
}
