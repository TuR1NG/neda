export interface VerifyCodeRequest {
  phone: string;
  code: number;
}
