export interface UserProfileCreateParam {
  UserId: string;
  FirstName: string;
  LastName: string;
  Phone: string;
  Picture: string;
  Gender: string;
}
